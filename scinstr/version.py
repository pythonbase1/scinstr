# -*- coding: utf-8 -*-
# scinstr/version.py

__version__ = "0.4.1"

# 0.4.1  (29/02/2024): Add subpakage 'vnav, 'compressor' and 'phimeter'.
#                      Major update of tctrl subpakage. Also a lot of code
#                      review, bug corrections and code cleaning.
# 0.4.0  (07/04/2023): Separate script files, now in scinstr-bin package,
#                      from driver files.
# 0.3.0  (06/03/2020): - Divide package into subpackage.
#                      - Name of signals use snake case instead of camel case.
# 0.2.0  (13/12/2019): Add support of signal/slot facilities in pure python
#                      using signalslot package.
# 0.1.3  (01/10/2019): Update daq34972a.py to PyQt5.
# 0.1.2  (19/03/2019): Move to PyQt5
# 0.1.1  (14/03/2019): Lot of modifications...
# 0.1.0a0 (18/09/2018): Initial version
