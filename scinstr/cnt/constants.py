# -*- coding: utf-8 -*-

# Default Agilent/Keysight 532x0A counter parameter
CNT532X0A_ID = "Technologies,532"
CNT532X0A_PORT = 5025
CNT532X0A_TIMEOUT = 3.0
CNT532X0A_VID = 0x0957
CNT532X0A_PID = 0x1707
