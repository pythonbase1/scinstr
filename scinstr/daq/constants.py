# -*- coding: utf-8 -*-

# Default Keysight DAQ 34972A parameters
DAQ34972A_ID = "Agilent Technologies,34972A"
DAQ34972A_PORT = 5025
DAQ34972A_TIMEOUT = 3.0

